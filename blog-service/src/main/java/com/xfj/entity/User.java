package com.xfj.entity;

import lombok.Data;

/**
 * @author ZQ
 */
@Data
public class User {
    private Long id;
    private String username;
    private String password;
}
