package com.xfj.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ZQ
 */

@Entity
@Data
public class Blog implements Serializable {
    private static final long serialVersionUID = 6913348275714703772L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String username;
    @Column
    private String title;
    @Column
    private String suject;
}
