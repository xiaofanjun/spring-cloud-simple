package com.xfj.dto;


import com.xfj.entity.Blog;
import com.xfj.entity.User;
import lombok.Data;

/**
 * @author ZQ
 */
@Data
public class BlogDetailDTO {
    private Blog blog;
    private User user;
}
