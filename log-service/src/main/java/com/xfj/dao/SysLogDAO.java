package com.xfj.dao;


import com.xfj.entity.SysLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ZQ
 */
public interface SysLogDAO extends JpaRepository<SysLog, Long> {
}
