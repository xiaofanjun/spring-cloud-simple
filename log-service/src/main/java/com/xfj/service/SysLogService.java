package com.xfj.service;

import com.xfj.dao.SysLogDAO;
import com.xfj.entity.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ZQ
 */
@Service
public class SysLogService {

    @Autowired
    SysLogDAO sysLogDAO;

    public void saveLogger(SysLog sysLog) {
        sysLogDAO.save(sysLog);
    }
}
