package com.xfj.dto;

import java.io.Serializable;

/**
 * 响应模型
 *
 * @author ZQ
 */
public class RespDTO<T> implements Serializable {

    public int code = 0;
    public String error = "";
    public T data;

    public static RespDTO onSuc(Object data) {
        RespDTO resp = new RespDTO();
        resp.data = data;
        return resp;
    }

    @Override
    public String toString() {
        return "RespDTO{" +
                "code=" + code +
                ", error='" + error + '\'' +
                ", data=" + data +
                '}';
    }
}
