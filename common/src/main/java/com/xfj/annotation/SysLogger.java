package com.xfj.annotation;

import java.lang.annotation.*;

/**
 * 日志
 *
 * @author ZQ
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLogger {
    String value() default "";
}
