package com.xfj.client.hystrix;


import com.xfj.client.AuthServiceClient;
import com.xfj.entity.JWT;
import org.springframework.stereotype.Component;

/**
 * 用户认证熔断器
 *
 * @author ZQ
 */
@Component
public class AuthServiceHystrix implements AuthServiceClient {
    @Override
    public JWT getToken(String authorization, String type, String username, String password) {
        System.out.println("--------opps getToken hystrix---------");
        return null;
    }
}
