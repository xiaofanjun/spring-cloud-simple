package com.xfj.dto;

import com.xfj.entity.User;
import lombok.Data;

@Data
public class LoginDTO {
    private User user;
    private String token;
}
