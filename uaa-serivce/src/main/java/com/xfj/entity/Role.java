package com.xfj.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * @author ZQ
 */
@Entity
@Data
public class Role implements GrantedAuthority {
    private static final long serialVersionUID = 7506672854606577122L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;


    @Override
    public String getAuthority() {
        return name;
    }

}
